#ifndef RECTANGLE_H
#define RECTANGLE_H 1

#include "Figure.h"

class Rectangle : public Figure {
 public:
  Rectangle();
  explicit Rectangle(int sideA, int sideB);
  void surfaceArea();
  void Perimeter();

 private:
  int m_sideA;
  int m_sideB;
};

#endif  // RECTANGLE_H