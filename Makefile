CC = g++

all.o: main.cpp Figure.o Rectangle.o
	$(CC) $^ -o main

Rectangle.o: Rectangle.cpp Rectangle.h Figure.h 
	$(CC) $< -c -o $@

Figure.o: Figure.cpp Figure.h
	$(CC) $< -c -o $@

clean:
	rm main