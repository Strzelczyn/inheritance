#ifndef FIGURE_H
#define FIGURE_H 1

class Figure {
 public:
  Figure();
  virtual void surfaceArea() = 0;
  virtual void Perimeter() = 0;
  int getsurfaceArea();
  int getPerimeter();

 public:
  int m_surfaceArea;
  int m_Perimeter;
};

#endif  // FIGURE_H
